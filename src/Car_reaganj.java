package lab3Start;

   /**
    * Note: Some Lab3 starter code provided by Dr. Sherri Harms of UNK.
    * This program represents a car that can accelerate, brake, and move.
    * Associated mutator and accessor methods have been established below.
    * This class is used in the RaceTest_reaganj.java program.
    * @author  John Reagan
    * @version 1.0, 02/23/2021
    */

class Car {
    private static final int DEFAULTMAXSPEED = 100;
    // declare private member variables here
    private String owner;
    private double currentSpeed;        // in terms of mph
    private double distanceTraveled;    // in terms of miles
    private double tripTime;            // in terms of minutes
    private double maxSpeed;            // in terms of mph

    /**
     * Constructor initializes a car
     * @param newOwner      The owner of the car.
     * @param newMaxSpeed   The max speed of the car.
     */
    public Car(String newOwner, double newMaxSpeed) {
        this.setOwner(newOwner);
        this.setCurrentSpeed(0);
        this.setMaxSpeed(newMaxSpeed);
        this.setDistanceTraveled(0.0);
        this.setTripTime(0);
    }

    /**
     * Copy constructor
     * @param another       Another car to be copied.
     */
    public Car(Car another) {
        this.setOwner(another.getOwner());
        this.setCurrentSpeed(another.getCurrentSpeed());
        this.setMaxSpeed(another.getMaxSpeed());
        this.setDistanceTraveled(another.getdistanceTraveled());
        this.setTripTime(another.getTripTime());
    }

    /**
     * Default constructor
     */
    public Car() {
        this("no owner", DEFAULTMAXSPEED);
    }

    /**
     * Move a car the distance it can travel at its current speed for 1 minute.
     */
    public void move() {
         distanceTraveled += currentSpeed / 60.0;   // mph * (1h/60.0 min)
         tripTime++;
    }

    /**
     * The accelerate method adds 5 miles per hour to current speed.
     * Remember: The most the current speed can be is the maxSpeed of the car
     */
    public void accelerate() {
        currentSpeed += 5;
        if (currentSpeed > maxSpeed)        // car is at maxSpeed
            currentSpeed = maxSpeed;
    }

    /**
     * The brake method subtracts 5 miles per hour from current speed.
     * Remember: The minimum speed for the current speed is 0
     */
    public void brake() {
        currentSpeed -= 5;
        if (currentSpeed < 0)               // car will stop
            currentSpeed = 0;
    }

    /**
     * Accessor getCurrentSpeed gets the currentSpeed parameter.
     * @return The current speed of the car.
     */
    public double getCurrentSpeed() {
        return currentSpeed;
    }

    /**
     * Mutator setCurrentSpeed sets the currentSpeed parameter.
     * set current speed - should not be used except by copy constructor
     * typically car is moved, rather than current speed just "set"
     * @param inSpeed       Set the car's current speed, in mph.
     */
    public void setCurrentSpeed(double inSpeed) { currentSpeed = inSpeed; }

    /**
     * Accessor getMaxSpeed gets the maxSpeed parameter.
     * @return The max speed of the car.
     */
    public double getMaxSpeed() {
        return maxSpeed;
    }

    /**
     * Mutator setMaxSpeed sets the maxSpeed parameter.
     * @param speed         Set the car's max speed, in mph.
     */
    public void setMaxSpeed(double speed) {
        maxSpeed = speed;
    }

    /**
     * Accessor getDistanceTraveled gets the distanceTraveled parameter.
     * @return The distance traveled for the car.
     */
    public double getdistanceTraveled() {
        return distanceTraveled;
    }

    /**
     * Mutator setDistanceTraveled sets the distanceTraveled parameter.
     * only used on constructors; typically "move" changes distance traveled
     * @param distance      Set the car's distance traveled, in miles.
     */
    public void setDistanceTraveled(double distance) {
        distanceTraveled = distance;
    }

    /**
     * Accessor getTripTime gets the tripTime parameter.
     * @return The value of the tripTime field.
     */
    public double getTripTime() { return tripTime; }

    /**
     * Mutator setTripTime sets the tripTime parameter.
     * @param time          Set the car's trip time, in minutes.
     */
    public void setTripTime(double time) {
        tripTime = time;
    }

    /**
     * Accessor getOwner gets the owner parameter.
     * @return The value of the owner field.
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Mutator setOwner sets the owner parameter.
     * @param newOwner      The car's new owner.
     */
    public void setOwner(String newOwner) {
        owner = newOwner;
    }

    /**
     * Displays the state of the Car object.
     * @return The car parameters as a String.
     */
    public String toString() {
         String carValue = String.format("%12s's car", owner) +
             " current speed: " + currentSpeed + " MPH, distance traveled: " +
             String.format("%8.2f",distanceTraveled) +
             " miles, trip time in minutes: " + tripTime;
        return carValue;
    }

    /**
     * reset the distance traveled, to start a new race.
     */
    public void resetDistance() {
        distanceTraveled = 0;
    }

    /**
     * reset the speed, to start a new race.
     */
    public void resetCurrentSpeed() {
        currentSpeed = 0;
    }

    /**
     * reset the tripTime, to start a new race.
     */
    public void resetTripTime() {
        tripTime = 0;
    }
}
