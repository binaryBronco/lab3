package lab3Start;

    /**
     * Note: Most of RaceTest starter code provided by Dr. Sherri Harms of UNK.
     * RaceTest tests the the Race object class with multiple method scenarios.
     * @author  John Reagan
     * @version 1.0, 02/23/2021
     */

class RaceTest {
    /** @param args */
    public static void main(String[] args) {
        // First Race test statements:
        lab3Start.Car sherriCar = new lab3Start.Car("Sherri", 85);
        lab3Start.Car labPartnerCar = new lab3Start.Car("Bozo", 100);
        lab3Start.Car yourCar = new lab3Start.Car("John", 100);


        lab3Start.Race firstRace = new lab3Start.Race();
        firstRace.setDistance(500);
        firstRace.addCar(sherriCar);
        firstRace.addCar(labPartnerCar);
        firstRace.addCar(yourCar);
        firstRace.runRace();

        System.out.println("The results of the first race are: ");
        System.out.println(firstRace.toString());
        System.out.println("_______________________");

        // Second Race test statements:
        // a copy of the race - lets race with copies of the cars!
        lab3Start.Race secondRace = new lab3Start.Race();
        secondRace.setDistance(500);
        lab3Start.Car[] carsCopy = firstRace.getCars();
        for (int i = 0; i < carsCopy.length; i++) {
            lab3Start.Car copy = carsCopy[i];
            // first we will modify the cars, making them faster
            copy.setMaxSpeed(copy.getMaxSpeed() * 2);
            copy.setOwner(copy.getOwner()+" COPY");
            copy.resetCurrentSpeed();   // reset copy car speed to 0.0 MPH
            copy.resetDistance();       // reset copy car distance to 0.00 miles
            copy.resetTripTime();       // reset copy car tripTime to 0 minutes
            secondRace.addCar(copy);    // add the copy car to the race
            System.out.println("Copied car:\t\t" + copy);
        }

        lab3Start.Car anotherCar = new lab3Start.Car("Santa", 100);
        secondRace.addCar(anotherCar);
        System.out.println("Additional car: " + anotherCar);
        System.out.println("_______________________");

        // check winner at beginning of race
        System.out.println("Before another race, the winner should be NULL.");
        System.out.println("Second race winner: " + secondRace.getWinner());
        System.out.println("_______________________");

        secondRace.runRace();

        System.out.println("The results of the second race are: ");
        System.out.println(secondRace.toString());
        System.out.println("_______________________");

        System.out.println("To verify that the second race did not change the" +
                        " cars in the first race.");
        lab3Start.Car[] carsInFirstRace = firstRace.getCars();
        for (int i = 0; i < firstRace.getCarCount(); i++) {
            System.out.printf("%-12s\tdistanceTraveled was %8.2f miles\n",
                    carsInFirstRace[i].getOwner(),
                    carsInFirstRace[i].getdistanceTraveled());
        }
        System.out.println("_______________________");
        lab3Start.Car[] carsInSecondRace = secondRace.getCars();
        for (int i = 0; i < secondRace.getCarCount(); i++)
            System.out.printf("%-12s\tdistanceTraveled was %8.2f miles\n",
                    carsInSecondRace[i].getOwner(),
                    carsInSecondRace[i].getdistanceTraveled());

        // BONUS ONLY TEST Bad Race test statements:
        System.out.println("_______________________");
        try {   // test out our exception
            lab3Start.Race thirdRace = new lab3Start.Race(-3);
            thirdRace.addCar(sherriCar);
            System.out.println("The status of the third race is: ");
            System.out.println(thirdRace.toString());
        }
        catch (lab3Start.NegativeStartingCount e) {
            System.out.println(e.getMessage());
        }
    }
}
