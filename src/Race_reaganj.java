package lab3Start;

    /**
     * Note: Some Race class starter code provided by Dr. Sherri Harms of UNK.
     * This program simulates a race of cars stored in an array.
     * Uses the dice class to decide which car and what action to take as the
     * race progresses.
     * @author  John Reagan
     * @version 1.0, 02/23/2021
     */

class Race {
    private static final double DEFAULTRACEDISTANCE = 500;
    private lab3Start.Car[] raceCars;
    private double distance;
    private lab3Start.Car winner;   //starts as null - when the race is not finished.
    private int carCount;   //Actual number of cars in the race. It ranges from 0 to the size of the array.

    /**
     * Constructor to make a default array of 3 cars. Set carCount =0. Set
     * distance to the default distance. Winner is null.
     * Be careful - each car is NULL at this point.
     */
    public Race() {
        raceCars = new lab3Start.Car[3];    // each raceCar in the array is NULL
        carCount = 0;
        distance = DEFAULTRACEDISTANCE;
        winner = null;
    }

    /**
     * Constructor to create the race that can handle inMaxCars number of cars.
     * See default constructor for other variables.
     * @param inMaxCars     The starting number of cars in the race.
     * @exception NegativeStartingCount     When inMaxCars is negative.
     */
    public Race(int inMaxCars) throws lab3Start.NegativeStartingCount {
        if (inMaxCars > 0)
            raceCars = new lab3Start.Car[inMaxCars];    // each raceCar is NULL
        else if (inMaxCars == 0)
            raceCars = null;
        else
            throw new lab3Start.NegativeStartingCount(inMaxCars);
        carCount = 0;
        distance = DEFAULTRACEDISTANCE;
        winner = null;
    }

    /**
     * Mutator setDistance to set the distance of the race.
     * @param inDistance    The distance of the race.
     */
    public void setDistance(double inDistance) { this.distance = inDistance; }

    /**
     * this is a PRIVATE method only called from other methods in this class
     * This does NOT need to be a deep copy, since the winning car of the race
     * will truly be one of the cars in the array
     * Thus, simply set the winner to the car in the array[winningCarIndex]
     * @param winningCarIndex   The index in the array of the winning car.
     */
    private void setWinner(int winningCarIndex) {
        this.winner = raceCars[winningCarIndex];
    }

    /**
     * Accessor getWinner returns the winning car as a Car.toString literal. If
     * the winner is unknown, return "Race winner is unknown at this time."
     * @return The toString output of the winning car.
     */
    public String getWinner() {
        if (winner != null)
            return "<WINNER!>\n" + winner.toString();
        else
            return "Race winner is unknown at this time.";
        }

    /**
     * Accessor getCarCount returns the number of actual cars in the race.
     * @return The number of cars in the race.
     */
    public int getCarCount() { return carCount;}

    /**
     * Add a car to the race after verifying that you have space in the array.
     * If not, you will need to increase the size of the array by calling the
     * private helper method increaseSize(). Be sure to add a COPY of the car
     * and be sure to increment the carCount
     * @param copyCar       A car to be added to the raceCars array.
     */
    public void addCar(lab3Start.Car copyCar) {
        if (raceCars == null || carCount == raceCars.length)
            increaseSize();
        // put the new raceCar in the next open spot
        raceCars[carCount] = new lab3Start.Car(copyCar);
        carCount++;
    }

    /**
     * Accessor getDistance returns the distance of the race.
     * @return The distance of the race.
     */
    public double getDistance() { return distance; }

    /**
     * Accessor getCars returns a DEEP copy of the array of cars. Each car in
     * the copied array is a copy of the car in the race's array of cars.
     * @return The new DEEP copy of the raceCars array.
     */
    public lab3Start.Car[] getCars() {
        lab3Start.Car[] temp = new lab3Start.Car[carCount];
        for (int i=0; i<carCount; i++)
            temp[i] = new lab3Start.Car(raceCars[i]);
        //	temp[i] = raceCars[i]; //shallow copy
        return temp;
    }

    /**
     * toString method, displays the state of the Race object.
     * @return The toString output for each Car in the raceCars array.
     */
    public String toString() {
        String raceValue = "";
        for (int index = 0; index < this.carCount; index++) {
            raceValue += this.raceCars[index].toString() + "\n";
        }
        raceValue += getWinner();
        return raceValue;
    }

    /**
     * runRace is a void method used to simulate running the race and set the
     * winner. To simulate the race, this method rolls a Dice object to select
     * the current action, until one of the cars reaches the finish line.
     */
    public void runRace() {
        lab3Start.Dice raceDice = new lab3Start.Dice(carCount*3);
        double finishLine = getDistance();
        while(winner == null) {
            int diceRoll = raceDice.roll();
            int carToMove = (diceRoll-1) / 3;
            int action = (diceRoll-1) % 3;
            // Switch action based on the diceRoll.
            switch (action) {
                case 0:
                    raceCars[carToMove].move();
                    break;
                case 1:
                    raceCars[carToMove].brake();
                    break;
                case 2:
                    raceCars[carToMove].accelerate();
                    break;
                default:
                    System.out.println("Check the switch. There is a bug.");
            }
            if (raceCars[carToMove].getdistanceTraveled() >= finishLine)
                setWinner(carToMove);
        }
    }

    //-----------------------------------------------------------------
    /**
     *     Private method
     *     Doubles the size of the raceCars array by creating a larger array
     *     and copying the existing raceCars into it.
     */
     private void increaseSize() {
         if (raceCars == null) {
             raceCars = new lab3Start.Car[2]; //double the size from 0 to 2
             for (int i = 0; i<2; i++)
                 raceCars[i] = new lab3Start.Car();
         } else {
             lab3Start.Car[] temp = new lab3Start.Car[raceCars.length * 2];

             for (int raceCar = 0; raceCar < raceCars.length; raceCar++)
                 temp[raceCar] = raceCars[raceCar];

             raceCars = temp;
         }
    }
}
